from django.shortcuts import render, get_object_or_404
from .models import Category, Company, Comment, Product
from django.views.generic import CreateView, UpdateView, DeleteView, ListView, DetailView, TemplateView
from django.http import HttpResponse, HttpResponseRedirect
from django.contrib.auth.mixins import LoginRequiredMixin, UserPassesTestMixin
from . forms import CommentForm
from django.views.generic.edit import FormMixin

from requests import get
import geoip2.database
# Create your views here.
def home(request):
    return HttpResponse("<H1> Hello welcome to my page </H1>")

class CategoryList(ListView):
    model = Category
    context_object_name = 'companies'
    template_name = 'categories/category_list'
    paginate_by = 2

class CompanyList(ListView):
    model = Company
    context_object_name = 'companies'
    template_name = 'categories/company_list.html'


    def get_queryset(self):
        self.category = get_object_or_404(Category, name = self.kwargs['category'])
        return Company.objects.filter(category=self.category)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["category"] = self.category
        return context

    def orderby_distance(self):
        dicti = []
        for company in Company.object.all():
            dist = company.get_distance()
            name = company.name
            dist_new = {
                'name' : name,
                'distance': dist,
            }
            dicti.append(dist_new)

        return HttpResponse(dicti)


class CompanyCreateView(LoginRequiredMixin, CreateView):
    model = Company
    template_name = 'categories/company_create.html'
    fields = '__all__'

    def form_valid(self, form):
        form.instance.owner = self.request.user
        return super().form_valid(form)


class CompanyUpdateView(LoginRequiredMixin, UserPassesTestMixin, UpdateView):
    model = Company
    template_name = 'categories/company_update.html'
    fields = ['name', 'description']

    def form_valid(self, form):
        form.instance.owner = self.request.user
        return super().form_valid(form)

    def test_func(self):
        company = self.get_object()
        if self.request.user == company.owner:
            return True
        return False


class CompanyDeleteView(LoginRequiredMixin, UserPassesTestMixin, DeleteView):
    model = Company
    template_name = 'categories/company_delete.html'
    success_url = '/categories'
    def test_func(self):
    	company = self.get_object()
    	if self.request.user == company.owner:
    		return True
    	return False

def search(request):
    company_list = Company.objects.all()
    company_filter = CompanyFilter(request.GET, queryset=company_list)
    return render(request, 'categories/company_search.html', {'filter': company_filter})


class CompanyDetailView(FormMixin, DetailView):
    model = Company
    template_name = 'categories/company_detail.html'
    form_class = CommentForm

    def get_success_url(self):
        #return reverse('categories/company_detail', kwargs={'category': self.object.category ,'pk':self.object.pk})
        return HttpResponseRedirect(self.request.path_info)

    def get_context_data(self, **kwargs):
        context = super(CompanyDetailView, self).get_context_data(**kwargs)
        context['form'] = CommentForm
        return context



    def form_valid(self, form):
        review = form.save(commit=False)
        review.created_by = self.request.user
        review.created_at = timezone.now()
        review.company = self.object.id
        review.save()
        return super(CompanyDetailView, self).form_valid(form)


def get_ip(request):
    ip = get('https://api.ipify.org').text
    reader = geoip2.database.Reader('categories/GeoLite2-City.mmdb')
    response = reader.city(ip)
    currrent_lat = response.location.latitude
    current_lon = response.location.longitude
    
    
    html = "<html><body> %s </body></html>" %ip
    return HttpResponse(html)

class ProductCreateView(CreateView):
    model=Product
    template_name='categories/product_new.html'
    fields=['name', 'company']



    def form_valid(self, form):
        form.instance.owner = self.request.user
        return super().form_valid(form)

    def test_func(self):
        company = self.get_object()
        if self.request.user == company.owner:
            return True
        return False



class AboutView(TemplateView):
    template_name = 'categories/aboutknowhere.html'

class AboutUsView(TemplateView):
    template_name = 'categories/aboutus.html' 
